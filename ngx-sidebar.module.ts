import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { NgxSidebarComponent } from './ngx-sidebar.component';
import { NgxMaterialModule } from '@4geit/ngx-material-module';

@NgModule({
  imports: [
    CommonModule,
    NgxMaterialModule,
    RouterModule,
  ],
  declarations: [
    NgxSidebarComponent
  ],
  exports: [
    NgxSidebarComponent
  ]
})
export class NgxSidebarModule { }
